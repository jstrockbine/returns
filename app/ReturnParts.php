<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnParts extends Model
{
	protected $table = 'return_parts';

	
   public function rma()
   {
   		return $this->belongsTo('App\ReturnDetails', 'id', 'returnid');
   }
}
   