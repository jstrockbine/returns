<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnDetails extends Model
{
	protected $table = 'return';


	public function parts()
	{
		return $this->hasMany('App\ReturnParts','returnid');
	}

	
   
}


  
