<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ReturnDetails;
use App\ReturnParts;
use App\UPC;
use App\StantToMotorad;


class ReturnDetailsController extends Controller
{

    public function index()
    {
        return view('welcome');
    }


	public function saveReturnDetails(Request $request)
	{
			
	    
        $returndata = new ReturnDetails();
        $form = $request['form'];

       
        $returndata->customer = $form['customer'];
        $returndata->rma = $form['rma'];
        $returndata->rt = $form['rt'];
        $returndata->received = date_create_from_format('m/d/Y', $form['received']);
        $returndata->processed = date_create_from_format('m/d/Y', $form['processed']);
        $returndata->user = $form['user'];

        //Write to the table
        $returndata->save();

        $returnid = $returndata->id;

        return $returndata;
	}


    public function lookupPart(Request $request)
    {
        $upcScan = $request['scannedPart'];

       
        $partDetails = $request['form'];

        //Stant returns initially, so lookup part with stant upc and cross to motorad
        //returning motorad part info
        $upcData = UPC::where('upc', '=', $upcScan)->first();

        if ($upcData['brand'] == 'stant')
        {

            //stant part so cross stant to motorad
            $stantPart = StantToMotorad::where('stant_part_number', '=', $upcData['part_number'])->first();

            $motoradCross = $stantPart['motorad_part_number'];
            $partName = $upcData['part_name'];
        }
        elseif ($upcData['brand'] == 'motorad')
        {
            //this will need to be updated if other brands are introduced besides motorad or stant
            //motorad part
            $motoradCross = $upcData['part_number'];
            $partName = $upcData['part_name'];
            $brand = "NOT FOUND";
            $comments = "";
            $quantity = "";
        }
        else
        {
            //no part found from upc scan
            $motoradCross = "not-found";
            $partName = "NO PART FOUND";
            $brand = "NOT FOUND";
            $comments = "";
            $quantity = "";
        }



       
        if($upcData)
        {
            $partNumber = $motoradCross;
            $brand = $partDetails['brand']; 
            $partName = $partName;
            $quantity = $partDetails['quantity'];
            $comments = $partDetails['comments'];
        }
       
       
        

        return ['partNumber' => $motoradCross, 'brand' => $brand, 'partName' => $partName, 'partComments' => $comments, 'partQuantity' => $quantity];
    }


    public function verifyAndAdd(Request $request)
    {

        $returnPart = new ReturnParts();
        $partToAdd = $request['addPart'];
        $partToAddQty = $request['partqty'];
        $partToAddComments = $request['partcomments'];
        $partToAddBrand = $request['partbrand'];
       
        $returnData = ReturnDetails::latest()->first();

        $returnPart->returnid = $returnData['id'];
        $returnPart->part_number = $partToAdd['partNumber'];
        $returnPart->rma = $returnData['rma'];
        $returnPart->rt = $returnData['rt'];
        $returnPart->comments = $partToAddComments;
        $returnPart->quantity = $partToAddQty;
        $returnPart->brand  = $partToAddBrand;
        $returnPart->user = $returnData['user'];
        $returnPart->customer = $returnData['customer'];
        $returnPart->processed = $returnData['processed'];
        $returnPart->received = $returnData['received'];

        $returnPart->save();


        return $returnPart;
    }


	
}