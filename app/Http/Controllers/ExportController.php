<?php

namespace App\Http\Controllers;

use App\ReturnParts;
use App\ReturnDetails;


use Illuminate\Http\Request;

use App\Exports\ReturnExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
   

    public function exportReturn(Request $request)
    {
        $returnData = ReturnDetails::latest()->first();
        $returnId = $returnData['id'];
        $rma = $returnData['rma'];

    	

    	return (new ReturnExport)->withId($returnId)->download('rma-'.$rma.'-'.date("m.d.Y").'.xlsx');
    }

   
}
