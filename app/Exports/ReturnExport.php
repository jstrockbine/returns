<?php

namespace App\Exports;

use App\ReturnDetails;
use App\ReturnParts;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;


class ReturnExport implements FromCollection, WithHeadings, WithMapping, WithStrictNullComparison
{
    use Exportable;

    public function withId(int $id)
    {
        

        $this->id = $id;
        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $returndetails= ReturnDetails::find($this->id);
        $full = $returndetails->parts;

       

        return $full;
    }

    public function map($rma) : array{
        return [ 
            $rma->customer,
            $rma->rma,
            $rma->rt,
            $rma->received,
            $rma->processed,
            $rma->user,
            $rma->part_number,
            $rma->quantity,
            $rma->brand,
            $rma->comments
            
            
        ];
    }

    public function headings(): array
    {
        return 
        [
            'Customer',
            'RMA #',
            'RT #',
            'Received Date',
            'Processed Date',
            'Processed By',
            'Part Number',
            'Quantity',
            'Brand',
            'Comments'
           
            
        ];
    }


   
}
