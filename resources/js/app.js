/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.$ = window.jQuery = require('jquery'); 
require('bootstrap');
//require('../vendor/jquery-easing/jquery.easing.min.js');
//require('../vendor/datatables/jquery.dataTables.min.js');
//require('../vendor/datatables/dataTables.bootstrap4.min.js');
//require('./sb-admin-2.js');

NodeList.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];


window.Vue = require('vue');
import axios from 'axios';
import 'es6-promise/auto';

import Vuex from 'vuex';
Vue.use(Vuex)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import createPersistedState from "vuex-persistedstate";

//import ToggleButton from 'vue-js-toggle-button'
//Vue.use(ToggleButton)



// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer ' + Laravel.apiToken,
};
window.Vue.prototype.$http = axios;
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
function initialState () {
  return {
 		return: {
 			rma: '',
 			customer: '',
 			rt: '',
 			received: '',
 			processed: '',
 			user: ''
 			

 		},
 		part: {
 			partNumber: '',
 			brand: '',
 			partName: '',
 			quantity: '',
 			comments: ''
 		},

 		loading: {
 			status: false,
 			message: 'Returns Loading...'
 		},
 		alert: {
 			status: false,
 			message: '',
 			type: 'alert-success'
 		},
 		currentRoute: location.hash.substr(1),
    	
    	user: '',
    	formStatus: 'start',
    	formButtonText: 'Start Return',
    	
 	}
 }


 /**
  *
  * VUEX Store
  *
  */
 const store = new Vuex.Store({
 	state: initialState,
 	mutations: {
 		reset (state) {
 			 // acquire initial state
	      const s = initialState()
	      Object.keys(s).forEach(key => {
	      	if(key != 'user')
	      	{
	      		state[key] = s[key]
	      	}
	        
	      })
 		},
 		setReturnID (state, payload)
 		{
 			state.return.id = payload.id;
 			state.return.status = payload.status;
 		},
 		changeRoute (state, route) {
 			state.currentRoute = route
 		},


 		setLoading (state, payload) {
 			//toggle the loading state status

 			state.loading.status = !state.loading.status
 			//set the loading message
 			state.loading.message = payload.message

 		},
 		clearLoading (state, payload) {
 			//toggle the loading state status
 			if(state.loading.status)
 			{
	 			state.loading.status = false
	 			//set the loading message
	 			state.loading.message = payload.message
	 		}

 		},
 		setAlert (state, payload) 
 		{
 			
 			//toggle the alert state status
 			state.alert.status = !state.alert.status
 			
 			//set the alert message and type
 			state.alert.message = payload.message
 			state.alert.type = payload.type

 			alert(state.alert.message);
 		},
 		
 		
 		setPartDetails (state, payload) {
 			state.part.partNumber = payload.partData.partNumber
 			state.part.brand = payload.partData.brand
 			state.part.partName = payload.partData.partName
 			state.part.comments = payload.partData.partComments
 			state.part.quantity = payload.partData.partQuantity
 		},

 		setReturnDetails (state, payload) {
 			state.return.customer = payload.returnDetails.customer
 			state.return.rma = payload.returnDetails.rma
 			state.return.rt = payload.returnDetails.rt
 			state.return.received = payload.returnDetails.received
 			state.return.processed = payload.returnDetails.processed
 			state.return.user = payload.returnDetails.user
 		},


 		
 		setUpReturn (state, payload) {

 			state.return = payload.return
 			state.parts = payload.parts
 			

			if(state.interchanges.missing.length > 0){
				state.interchanges.status = 'missing'
			}
			if(state.interchanges.pos.length > 0 && state.interchanges.missing.length === 0){
				state.interchanges.status = 'complete'
			}
 		},
 		
 		
 		

 	},
 	plugins:[createPersistedState()],
 });

import { mapState } from 'vuex';


//Vue.component('StartForm', require('./components/StartForm.vue').default);
Vue.component('TopAlert', require('./components/TopAlert.vue').default);
// Vue.component('mio-table', require('./components/MioTable.vue').default);
// Vue.component('mio-internal-view', require('./components/MioInternalView.vue').default);
// Vue.component('mio-charts', require('./components/MioCharts.vue').default);
//Vue.component('MioOutput', require('./components/mioOutput.vue').default);
//Vue.component('tween-num', require('vue-tween-number'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 // window.addEventListener('load', function () {
	
	const app = new Vue({
	    el: '#app',
	    store,
	    data() {
	    	return {
		    	showDataForm: true,
		    	returnForm: {
		    		rma: '',
	 				customer: '',
	 				rt: '',
	 				received: '',
	 				processed: '',
	 				user: '',
	 				comments: '',
	 				quantity: '',
	 				brand: '',
	 				error: {
	 					data: false
	 				}

	 				
		    	},
		    	
		    }
	    	
	    },


	     computed: {  
		      smid: {
		        get() {
		          return this.$store.state.savedmioid
		      	}
		 	},    
		 

      		...mapState(['currentRoute','user','return', 'loading', 'alert', 'part'])
	    },
	 
	    

	    	
	    //render (h) {return h(this.ViewComponent)},
	    created: function() {
	    	//get user profile info
	    	//this.getprofile();
	    },
	    watch: {
	    	//storeSize: function(size) {
	    		//this.calculateMIO();
	    	//}
	    },
	    methods:{
	    	reset: function() {
	    		var c = confirm('Are you sure you want to reset?');
	    		if(c) this.$store.commit('reset');
	    		this.route("")
	    		location.reload()
	    	},
	    	startnew: function() {
	    		this.$store.commit('reset');
	    		//location.replace("/start");
	    		this.route('');
	    	},
	    	logout: function() {
	    		this.reset
	    		location.replace("/signout");
	    	},
	    	route: function(route){

	    		window.history.pushState(route, route, '#'+route);
	    		this.$store.commit('changeRoute', route);
	    		
	    		
	    	},

	    
	    	
	    	routeChange: function() {
	    		var route = location.hash.replace('#','');
	    		this.$store.commit('changeRoute', route);
	    	},
	    	
	    	
	    	startNewReturn() {
	    		//Set loading message and Start calculations
	    		this.$store.commit('setLoading', {message: 'Saving intital Return info...'})
	    		this.route('lookupPart');
	    		
	    			
	    		
	    		//Send API call to server to get all calcualtions
	    		axios.post('/api/startNewReturn', {form: this.returnForm}).then((response) => {
	    			//for testing echo to console
	    			console.log(response);
	    			this.$store.commit('setReturnDetails', {returnDetails: response.data})
	    			
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'A new return has been started. Scan parts and add them below', type: 'alert-success'})
	    			
	    			//this.$store.commit('setReturn', response.data);
	    			
	    		}).catch((error) => {
	    			console.log(error)
	    			//alert error and clear loading
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'We have encountered an error:\n'+error.data.message, type: 'alert-danger'})
	    		});
	    	},

	    	lookupPart() {
	    		//Set loading message and Start calculations
	    		this.$store.commit('setLoading', {message: 'Looking up part...'})
	    		this.route('verifyAndAdd');
	    		
	    			
	    		
	    		//Send API call to server to get all calcualtions
	    		axios.post('/api/lookupPart', {scannedPart: this.returnForm.part}).then((response) => {
	    			//for testing echo to console
	    			console.log(response);
	    			this.$store.commit('setPartDetails', {partData: response.data})
	    			
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'Part found. Verify part below.', type: 'alert-success'})
	    			
	    			//this.$store.commit('setReturn', response.data);
	    			
	    		}).catch((error) => {
	    			console.log(error)
	    			//alert error and clear loading
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'We have encountered an error:\n'+error.data.message, type: 'alert-danger'})
	    		});
	    	},

	    	verifyAndAdd() {
	    		//Set loading message and Start calculations
	    		this.$store.commit('setLoading', {message: 'Adding part to return...'})
	    		this.route('finishAndExport');
	    		
	    		
	    		axios.post('/api/addParts', {addPart: this.part, partbrand: this.returnForm.brand, partqty: this.returnForm.quantity, partcomments: this.returnForm.comments}).then((response) => {
	    			//for testing echo to console
	    			console.log(response);
	    		
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'Part has been added. Add another or Export return data', type: 'alert-success'})
	    			
	    			//this.$store.commit('setReturn', response.data);
	    			
	    		}).catch((error) => {
	    			console.log(error)
	    			//alert error and clear loading
	    			this.$store.commit('setLoading', {message: 'Done'})
	    			this.$store.commit('setAlert', {message: 'We have encountered an error:\n'+error.data.message, type: 'alert-danger'})
	    		});
	    	},

	    	

	    	
	    	

	   
	    }
	  

	});




