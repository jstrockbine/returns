
<div class="sidebars-wrapper">

  <!--<div class="brandbar">
      <img src="/images/SVG/mio_icon_white.svg" width="48px" height="48px">
      <div class="brand_nav">
        <a class="logoutLink" @click.prevent="logout">
          <i class="fas fa-sign-out-alt fa-sm "></i><br>
          Logout
        </a>
          <img class="img-profile rounded-circle" :src="user.photo" width="48" height="48">


      </div>
    </div>-->
    <div>
        <ul class="navbar-nav  sidebar sidebar-light accordion " v-bind:class="{ toggled: sidebarCollapes }" id="accordionSidebar ">
          <!-- Sidebar - Brand -->
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-text mx-3">Returns360</div>
          </a>

          <!-- Divider -->
          <hr class="sidebar-divider my-0">

          <!-- Nav Item - Dashboard -->
          <li class="nav-item" :class="{ active: currentRoute ==='start' }">
            <a class="nav-link" v-on:click.prevent="route('start')">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Start</span></a>
          </li>
           
            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item" :class="{ active: currentRoute ==='summary' }">
              <a class="nav-link" v-on:click.prevent="route('summary')" >
                <i class="fas fa-fw fa-chart-bar"></i>
                <span>Summary</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" v-on:click.prevent="reset" >
                <i class="fas fa-fw fa-exclamation"></i>
                <span>Reset</span>
              </a>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            {{-- <li class="nav-item">
              <a class="nav-link" href="#" >
                <i class="fas fa-fw fa-wrench"></i>
                <span>Details</span>
              </a>
            </li> --}}

            <!-- Nav Item - Pages Collapse Menu -->
            {{-- <li class="nav-item">
              <a class="nav-link" href="#" >
                <i class="fas fa-fw fa-folder"></i>
                <span>Buyers Guide</span>
              </a>
            </li> --}}
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle" v-on:click="toggleSidebar"></button>
          </div>
        </ul>
      </div>
</div>
