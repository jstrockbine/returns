<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
       <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Returns</title>

  
        <link href="/css/app.css" rel="stylesheet">
        @yield('addToHeader')
  
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .top-left {
                position: absolute;
                left: 10px;
                top: 30px;
            }

            .tbl td
            {
                font-weight: 850;
                font-size: 15px;
                padding: 10px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 45px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
       <div id="app">
            <div align="center">
                <top-alert></top-alert>
            </div>
            <div class = 'top-left'>
                <h3>Return Details</h3>
                <hr>
                <form enctype = "multipart/form-data">
                    <table class = 'tbl' id='rma'>
                        <tr>
                            <td>Customer Name</td>
                            <td><input name="customer" v-model="returnForm.customer"></td>
                        </tr>
                         <tr>
                            <td>RMA #</td>
                            <td><input name="rma" v-model="returnForm.rma"></td>
                        </tr>
                         <tr>
                            <td>RT #</td>
                            <td><input name="rt" v-model="returnForm.rt"></td>
                        </tr>
                         <tr>
                            <td>Received Date<br>
                                <small><i>ex: 1/1/2020</i></small></td>
                            <td><input name="received" v-model="returnForm.received"></td>
                        </tr>
                         <tr>
                            <td>Processed Date<br>
                                <small><i>ex: 1/1/2020</i></small></td>
                            <td><input name="processed" v-model="returnForm.processed"></td>
                        </tr>
                         <tr>
                            <td>Processed By</td>
                            <td><input name="user" v-model="returnForm.user"></td>
                        </tr>
                        <tr>
                            <td colspan='2' align='right'><button type='submit' class="btn btn-success" v-on:click.prevent="startNewReturn">Start New Return</td>
                        <tr>
                            <td colspan='2' ><hr></td>
                        </tr>
                    </table>
                </form> <!-- end rma details form -->

                <transition name="fade">
                    <div v-if="currentRoute === 'lookupPart' || currentRoute === 'verifyAndAdd' || currentRoute === 'addParts' || currentRoute === 'finishAndExport'">
                        <table class='tbl' id='tblscan'>
                            <tr>
                                <td>
                                    <h4>Part Scan</h4>
                                    <small><i>make sure cursor is in<br>
                                    textbox before upc scan</i></small>
                                </td>
                                <td><input name='part' v-model='returnForm.part'></td>
                            </tr>
                             <tr>
                                <td colspan='2' align='right'><button class="btn btn-success" v-on:click.prevent="lookupPart">Lookup Part</button></td>
                            <tr>
                        </table>
                    </div>
                </transition>
                
            </div> <!-- end top left -->
            
            <div class="content" v-if="currentRoute === 'verifyAndAdd' || currentRoute === 'addParts' || currentRoute === 'finishAndExport'">
                <div id="content-wrapper" class="d-flex flex-column">
                   <h3>Verify Part and Add to Return</h3>
                    <div class='flex-center' style="margin-left: 70px;">
                        <img :src="'/images/catalog/pictures/products/' + part.partNumber + '.jpg'" style="width: 410px; height: 410px; margin-top: 35px">
                        <div style="margin-top: 20px;">
                             <table class='tbl' id='partDetails'>
                                 <tr>
                                    <td>
                                      Part Number:
                                    </td>
                                    <td>@{{part.partNumber}}</td>
                                </tr>
                                <tr>
                                    <td>
                                      Part Description:
                                    </td>
                                    <td>@{{part.partName}}</td>
                                </tr>
                                <tr>
                                    <td>
                                      Brand:
                                    </td>
                                    <td><input name='brand' v-model='returnForm.brand'></td>
                                </tr>
                                <tr>
                                    <td>
                                      Quantity:
                                    </td>
                                    <td><input name='quantity' v-model='returnForm.quantity'></td>
                                </tr>
                                <tr>
                                    <td>
                                      Comments:
                                    </td>
                                     <td><textarea name='comments' v-model='returnForm.comments'></textarea></td>
                                </tr>
                                 <tr>
                                    <td colspan='2' align='right'><button class="btn btn-success" v-on:click.prevent="verifyAndAdd">Verify and Add</button></td>
                                </tr>
                                <tr>
                                    <td colspan='2' style="margin-bottom: 20px;"><hr></td>
                                <tr>
                                    <td align='left'><button class="btn btn-danger" v-on:click.prevent="reset">Reset - Start New</button></td>
                                    <td align='right'>
                                        <div v-if="currentRoute === 'finishAndExport'">
                                           <a v-bind:href = "/export/" class="btn btn-secondary">Export Return Data</a>
                                        </div>
                                    </td>

                            </table>

                            
                      </div>
                      
                    </div>
                   
                </div>
                
            </div>

                
        </div>
    <script>
   window.Laravel = {!! json_encode([
       'apiToken' => $currentUser->api_token ?? null,
   ]) !!};
</script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
  <script src="/js/app.js" ></script>
  <script src="https://kit.fontawesome.com/ea23f17b47.js"></script>
    
    </body>
</html>
