<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Returns360 - @yield('title')</title>

  <!-- Custom styles for this template-->
  
  <link href="/css/app.css" rel="stylesheet">
  @yield('addToHeader')
  
</head>

 <body id="page-top ">

  <!-- Page Wrapper -->
  <div id="app">
    <transition name="fade">
    <div v-cloak>
      <div id="wrapper" >
      

        @yield('sidebar_layout')
        <!-- Content Wrapper -->
        
        <div id="content-wrapper" class="d-flex flex-column" v-bind:class="{ toggled: sidebarCollapes }">
           <!-- <top-alert></top-alert>-->
          <!-- Main Content -->
          <div id="content">
            
              <!-- Begin Page Content -->
              @yield('content')
              <!-- End of Page Content -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
              <div class="container my-auto">
                <div class="copyright text-center my-auto">
                  <span>Copyright &copy; MotoRad 2019</span>
                </div>
               </div>
            </footer>
             <!-- End of Footer -->
        </div>
        <!-- End of Content Wrapper -->
      </div>
      <!-- End of Page Wrapper -->
    </div>
    </transition>
    <!--Loading icon while waiting for VUE to load -->
    <!--<transition name="fade">
      <div class="loading" v-if="!$data">
        <h1>MIO Loading..... <br><i class="fas fa-sync fa-spin fa-2x"></i></h1>
      </div>
    </transition>-->
    <!--===============================================================
    =            Loading Screen for inter app changes ( Move to component) =
    ================================================================-->
    <transition name="fade">
      <div class="loading" v-if="loading.status">
        <h1>@{{loading.message}}<br><img src="/images/engine8.svg"></h1>
      </div>
    </transition>
  </div>
  <!-- End of APP CONTANINER -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  
  <!-- Custom scripts for all pages-->
<script>
   window.Laravel = {!! json_encode([
       'apiToken' => $currentUser->api_token ?? null,
   ]) !!};
</script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
  <script src="/js/app.js" ></script>
  <script src="https://kit.fontawesome.com/ea23f17b47.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- Page level custom scripts -->
  @yield('addToFooter')
<script type="text/javascript" src="https://motorad.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-7f820x/b/19/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=f854aff3"></script>


</body>

</html>